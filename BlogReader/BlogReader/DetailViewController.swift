//
//  DetailViewController.swift
//  BlogReader
//
//  Created by Nik on 06/04/16.
//  Copyright © 2016 KirillGr. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var detailDescriptionLabel: UILabel!
    @IBOutlet var contentBlog: UIWebView!


    var detailItem: AnyObject? {
        didSet {
            // Update the view.
            self.configureView()
        }
    }

    func configureView() {
        if contentBlog != nil {
            let content_text = self.detailItem!.valueForKey("content")!.description
            contentBlog.loadHTMLString(content_text, baseURL: nil)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.configureView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

